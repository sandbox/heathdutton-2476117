/**
 * Handle events on the checkout page(s)
 */
(function ($) {
  Drupal.behaviors.JSONBehavior = {

    attach: function (context, settings) {

      /**
       * JS equivalent of the PHP function.
       * Used to convert the safe values Drupal provided back into the original.
       *
       * http://phpjs.org/functions/htmlspecialchars_decode/
       *
       * @param string
       * @param quote_style
       * @returns {*}
       */
      function htmlspecialchars_decode(string, quote_style) {

        var optTemp = 0,
            i = 0,
            noquotes = false;
        if (typeof quote_style === 'undefined') {
          quote_style = 2;
        }
        string = string.toString()
            .replace(/&lt;/g, '<')
            .replace(/&gt;/g, '>');
        var OPTS = {
          'ENT_NOQUOTES': 0,
          'ENT_HTML_QUOTE_SINGLE': 1,
          'ENT_HTML_QUOTE_DOUBLE': 2,
          'ENT_COMPAT': 2,
          'ENT_QUOTES': 3,
          'ENT_IGNORE': 4
        };
        if (quote_style === 0) {
          noquotes = true;
        }
        if (typeof quote_style !== 'number') {
          // Allow for a single string or an array of string flags
          quote_style = [].concat(quote_style);
          for (i = 0; i < quote_style.length; i++) {
            // Resolve string input to bitwise e.g. 'PATHINFO_EXTENSION' becomes 4
            if (OPTS[quote_style[i]] === 0) {
              noquotes = true;
            } else if (OPTS[quote_style[i]]) {
              optTemp = optTemp | OPTS[quote_style[i]];
            }
          }
          quote_style = optTemp;
        }
        if (quote_style & OPTS.ENT_HTML_QUOTE_SINGLE) {
          string = string.replace(/&#0*39;/g, "'"); // PHP doesn't currently escape if more than one 0, but it should
          // string = string.replace(/&apos;|&#x0*27;/g, "'"); // This would also be useful here, but not a part of PHP
        }
        if (!noquotes) {
          string = string.replace(/&quot;/g, '"');
        }
        // Put this in last place to avoid escape being double-decoded
        string = string.replace(/&amp;/g, '&');

        return string;
      }

      /**
       * Given a JSON string, add syntax highlighting tags for styling.
       *
       * @param json
       * @returns {*}
       */
      function jsonSyntaxHighlight(json) {
        json = json.replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;');
        return json.replace(/("(\\u[a-zA-Z0-9]{4}|\\[^u]|[^\\"])*"(\s*:)?|\b(true|false|null)\b|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?)/g, function (match) {
          var cls = 'number';
          if (/^"/.test(match)) {
            if (/:$/.test(match)) {
              cls = 'key';
            } else {
              cls = 'string';
            }
          } else if (/true|false/.test(match)) {
            cls = 'boolean';
          } else if (/null/.test(match)) {
            cls = 'null';
          }
          return '<span class="' + cls + '">' + match + '</span>';
        });
      }

      /**
       * Given a jQuery object, convert the HTML to a JSON object.
       *
       * @param obj
       * @returns string
       */
      function jsonStringFromjQueryObj(obj) {
        var result = {};
        try {
          var jsonRaw = obj.is('pre') ? obj.html() : obj.text(),
              jsonCleaned = htmlspecialchars_decode(jsonRaw),
              jsonObj = JSON.parse(jsonCleaned),
              jsonString = JSON.stringify(jsonObj, null, 2);
          result = jsonString;
        } catch (e) {
        }
        return result;
      }

      /**
       * Find all text-json formatted fields, and pretty-print the json safely.
       */
      if (typeof JSON != 'undefined') {
        $(context).find('.text-json').each(function () {
          var jsonString = jsonStringFromjQueryObj($(this)),
              jsonPretty = jsonSyntaxHighlight(jsonString);
          $(this).html(jsonPretty);
        });
      }

      /**
       * If there are editable json widgets, initialize JSON Editor.
       * Note: Not using jQuery json-editor syntax as it may not be supported
       * in future versions (per author).
       */
      if (typeof JSON != 'undefined' && typeof JSONEditor != 'undefined') {

        var theme = settings.json.theme,
          iconlib = settings.json.iconlib;

        // Set defaults, if default mode is set
        if (theme == 'default'){
          theme = 'bootstrap3';
        }
        if (iconlib == 'default'){
          iconlib = 'bootstrap3';
        }

        // Do some basic auto-detection if desired.
        if (theme == 'auto' || iconlib == 'auto'){
          var autotheme = 'html',
            autolib = null;
          if (typeof $().emulateTransitionEnd == 'function'){
            autotheme = 'bootstrap3';
            autolib = 'bootstrap3';
          } else if (typeof $().modal == 'function') {
            autotheme = 'bootstrap2';
            autolib = 'bootstrap2';
          } else if (typeof jQuery.ui != 'undefined') {
            autotheme = 'jqueryui';
            autolib = 'jqueryui';
          } else if (typeof Foundation != 'undefined'){
            var majorVer = Foundation.version.split('.')[0];
            if (majorVer == '5'){
              autotheme = 'foundation5';
            } else if (majorVer == '4'){
              autotheme = 'foundation4';
            } else if (majorVer == '3'){
              autotheme = 'foundation3';
              autolib = 'foundation3';
            } else if (majorVer == '2'){
              autolib = 'foundation2';
            }
          }
          if (theme == 'auto'){
            theme = autotheme;
          }
          if (iconlib == 'auto'){
            iconlib = autolib;
          }
        }

        // Then activate JSON Editor
        $(context).find('textarea.text-json-widget').each(function() {

          try {
            var jsonRaw = $(this).text(),
                jsonCleaned = htmlspecialchars_decode(jsonRaw),
                jsonObj = JSON.parse(jsonCleaned);
          } catch (e) {
            if (typeof console != 'undefined' && typeof console.error != 'undefined'){
              console.warn('Could not decode the JSON completely: ' + e);
            }
          }

          if (typeof jsonObj != 'undefined'){
            var newObj = $('<pre class="text-json-widget">' + jsonRaw + '</pre>');
            $(this).css({'display': 'none'});
            $(this).after(newObj);
            setTimeout(function(){
              newObj.parent().removeClass('resizable-textarea');
              newObj.parent().find('.grippie').first().remove();
            }, 50);

            var element = newObj[0];
            new JSONEditor(element, {
              theme: theme,
              schema: {},
              startval: jsonObj,
              iconlib: iconlib,
              disable_collapse: true
            });
          }
        });
      }
    }
  }
})(jQuery);
