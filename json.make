; Drupal version
core = 7.x
api = 2

; Community dependency for JSON

libraries[json-editor][download][type] = git
libraries[json-editor][download][url] = https://github.com/jdorn/json-editor.git
libraries[json-editor][download][tag] = v0.7.28